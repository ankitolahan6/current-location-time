current location and timezone
please remove 'main' from the "current-location-time-main" Once downloaded.
This is a Drupal 9 module which creates a custom block plugin to show entered location and current time based on selected timezone.

Download code & Install the module.
Module configuration--> you can acces form via /admin/current-location-time and save your configuration by slecting your country city and timezone. 
Place the custom block "Result of current Location & Time" from Structure > Block layout in any region to view selected location and current date & time as per selected timezone.
