<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\ArticleBlock.
 */

namespace Drupal\current_location_time\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

/**
 * Provides a 'Location & Time' block.
 *
 * @Block(
 *   id = "location_form_block",
 *   admin_label = @Translation("Result of current Location & Time"),
 *   category = @Translation("Custom")
 * )
 */
class LocationFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $html = "";
    $data = \Drupal::service('current_location_time.LocationService')->getLocation();
    $config = \Drupal::config('current_location_time.location_user');
    $country = $config->get('country');

    $wrongcity = $config->get('city');   
    $whatIWant = substr($wrongcity, strpos($wrongcity, "/") + 1);    
	  $city = $whatIWant;
    $timezone = $config->get('timezone');
    $html = "<h2>Your Date/Time and Location details:</h2>";
    $html .= "<p><b>Country - </b>" . $country . "</p>";
    $html .= "<p><b>City - </b>" . $city . "</p>";
    $html .= "<p><b>Current Timezone - </b>" . $timezone . "</p>" . "<p><b> Current Date/Time - </b>" . $data . "</p>";


    return [
      '#markup' => $html,
      '#cache' => array(
        'tags' => [
          'TIME_USER'
        ]
      ),
    ];
  }
}