<?php
/**
 *  @file
 *  Contains \Drupal\rsvp_module\Form\RSVPForm
 */

namespace Drupal\current_location_time\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 *  Provides a form to get users time and location
 */
class LocationTimeForm extends ConfigFormBase {
   /**  
   * {@inheritdoc}  
   */  
  protected function getEditableConfigNames() {
    return [
      'current_location_time.location_user',
    ];
  }  

  /**  
   * {@inheritdoc}  
   */  
  public function getFormId() {
    return 'location_user';
  }


  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

   $options['date_format'] = ['default' => 'small'];
   $options['custom_date_format'] = ['default' => ''];
   $options['timezone'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $country_manager = \Drupal::service('country_manager');
    $list = $country_manager->getList();
    $countries = [];
    foreach ($list as $key => $value) {
      $val = $value->__toString();
      $countries[$key] = $val;
    }
   $config = $this->config('current_location_time.location_user');

    $form['country'] = [
      '#title' => $this->t('Country'),
        '#type' => 'select',
        '#default_value' => $config->get('country'),
        '#options' => [
          'country_select' => '-- Select the Countries --',  
          'country' => $countries,
        ],
      ];

    $form['city'] = [
      '#title' => $this->t('City'),
      '#type' => 'select',
      '#default_value' => $config->get('city'),
      '#options' => ['' => $this->t('--city--')] + system_time_zones(FALSE, TRUE),
      
    ];

    $form['timezone'] = [
        '#type' => 'select',
        '#title' => $this->t('Timezone'),
        '#default_value' => $config->get('timezone'),
        '#options' => ['' => $this->t('--timezone--')] + system_time_zones(),
        
      ];


      return parent::buildForm($form, $form_state);
  }


  /**
  * (@inheritdoc)
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {  
    parent::submitForm($form, $form_state);
    $this->config('current_location_time.location_user')
    ->set('country', $form_state->getValue('country'))
    ->set('city', $form_state->getValue('city'))
    ->set('timezone', $form_state->getValue('timezone'))
    ->save();
    \Drupal\Core\Cache\Cache::invalidateTags(array('TIME_USER'));
  }
}